<?php

namespace App\Controller;

use App\Controller\AppController;

class ImagesController extends AppController
{
    public function initialize(): void {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index', 'image', 'images', 'apiimage', 'apiimages']);
    }


    public function index()
    {
       $images = $this->Images->find()->all();

       $this->set(compact('images'));
    }
    public function image()
    {
        $tmp = $this->getRequest()->getQuery();
        if (!empty($tmp)){
            $var = array_values($tmp)[0];
        } else {
            $var = null;
        }
        if ($var !== null){
            $foundimage = false;
            $tab = glob(WWW_ROOT . 'img/jpg/*.jpg');
            foreach ($tab as $value) {
                if (pathinfo($value,PATHINFO_BASENAME) === $var) {
                    $foundimage = true;
                    $exif = exif_read_data($value, 0, true);
                    $MyObj = new \stdClass;
                    $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                    $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                    $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                    $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                    $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                    $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                    $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                }
            }
            $this->set(compact('MyObj'));
            if ($foundimage) {

            } else {
                return $this->response
                    ->withStatus(400);
            }
        }
    }


    public function images()
    {
        $limit = $this->getRequest()->getQuery("limit");
        $boollimit = false;
        if (isset($limit)) {
            $cpt = 0;
            $boollimit = true;
        }
        $name = $this->getRequest()->getQuery("name");
        $boolname = false;
        if (isset($name)) {
            $boolname = true;
        }
        $current = $this->getRequest()->getQuery("pages");
        if (!isset($current) | $current < 1) {
            $current = 1;
        }
        $tab3 = [];
        $tab2 = [];
        $tab = glob(WWW_ROOT . 'img/jpg/*.jpg');
        foreach ($tab as $value) {
            if ($boollimit) {
                if ($cpt >= $limit) {
                    break;
                }
                $cpt++;
            }
            if ($boolname) {
                $match = preg_match($name, pathinfo($value,PATHINFO_BASENAME));
                if ($match !== 0 & $match !== false) {
                    $exif = exif_read_data($value, 0, true);
                    $MyObj = new \stdClass;
                    $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                    $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                    $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                    $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                    $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                    $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                    $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                    $tab3[] = json_encode($MyObj);
                }
            } else {
                $exif = exif_read_data($value, 0, true);
                $MyObj = new \stdClass;
                $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                $tab3[] = json_encode($MyObj);
            }
        }
        $varimages = json_encode($tab3);
        $pages = $this->getRequest()->getQuery("pages");
        $this->set(compact('tab3', 'current', 'pages'));
    }

    public function apiimage()
    {
        $tmp = $this->getRequest()->getQuery();
        if (!empty($tmp)){
            $var = array_values($tmp)[0];
        } else {
            $var = null;
        }
        if ($var !== null){
            $foundimage = false;
            $tab = glob(WWW_ROOT . 'img/jpg/*.jpg');
            foreach ($tab as $value) {
                if (pathinfo($value,PATHINFO_BASENAME) === $var) {
                    $foundimage = true;
                    $exif = exif_read_data($value, 0, true);
                    $MyObj = new \stdClass;
                    $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                    $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                    $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                    $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                    $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                    $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                    $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                }
            }
            $this->set(compact('MyObj'));
            if ($foundimage) {
                return $this->response
                    ->withStringBody(json_encode($MyObj))
                    ->withStatus(200)
                    ->withType('application/json');
            } else {
                return $this->response
                    ->withStatus(400);
            }
        }
    }


    public function apiimages()
    {
        $limit = $this->getRequest()->getQuery("limit");
        $boollimit = false;
        if (isset($limit)) {
            $cpt = 0;
            $boollimit = true;
        }
        $name = $this->getRequest()->getQuery("name");
        $boolname = false;
        if (isset($name)) {
            $boolname = true;
        }
        $current = $this->getRequest()->getQuery("pages");
        if (!isset($current) | $current < 1) {
            $current = 1;
        }
        $tab3 = [];
        $tab2 = [];
        $tab = glob(WWW_ROOT . 'img/jpg/*.jpg');
        foreach ($tab as $value) {
            if ($boollimit) {
                if ($cpt >= $limit) {
                    break;
                }
                $cpt++;
            }
            if ($boolname) {
                $match = preg_match($name, pathinfo($value,PATHINFO_BASENAME));
                if ($match !== 0 & $match !== false) {
                    $exif = exif_read_data($value, 0, true);
                    $MyObj = new \stdClass;
                    $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                    $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                    $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                    $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                    $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                    $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                    $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                    $tab3[] = json_encode($MyObj);
                }
            } else {
                $exif = exif_read_data($value, 0, true);
                $MyObj = new \stdClass;
                $MyObj->file = (isset($exif['FILE']['FileName']) ? $exif['FILE']['FileName'] : null);
                $MyObj->description = (isset($exif['IFD0']['ImageDescription']) ? $exif['IFD0']['ImageDescription'] : null);
                $MyObj->comment = (isset($exif['COMPUTED']['UserComment']) ? $exif['COMPUTED']['UserComment'] : null);
                $MyObj->author =(isset($exif['MAKERNOTE']['OwnerName']) ? $exif['MAKERNOTE']['OwnerName'] : null);
                $MyObj->width = (isset($exif['COMPUTED']['Width']) ? $exif['COMPUTED']['Width'] : null);
                $MyObj->height = (isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Height'] : null);
                $MyObj->html = "<img src='$MyObj->file' alt='$MyObj->description'>";
                $tab3[] = json_encode($MyObj);
            }
        }
        return $this->response
            ->withStringBody(json_encode($tab3))
            ->withStatus(200)
            ->withType('application/json');
    }

    public function add()
    {
        $imageEntity = $this->Images->newEmptyEntity();
        if ($this->request->is('post')) {
            $fileobject = $this->getRequest()->getData("field");
            $uploadPath = WWW_ROOT . "img/jpg/";
            $destination = $uploadPath . $this->getRequest()->getData('name') . 'jpg';

            $this->Images->patchEntity($imageEntity,$this->getRequest()->getData());


            if ($fileobject != null && $this->Images->save($imageEntity)) {
                $filename = $fileobject->getClientFilename();

                if (!glob(WWW_ROOT . 'img' . DS . 'jpg' . DS . $filename)) {
                    $fileobject->moveTo(WWW_ROOT . 'img' . DS . 'jpg' . DS . $filename);
                    $this->Flash->success('Ajout effectué avec succès');
                }
                else {
                    $this->Flash->error('Fichier déjà existant');
                }
                return $this->redirect($this->referer());
            }
            else {
                $this->Flash->error('Une erreur est survenue');
            }

        }
        $this->set(compact('imageEntity'));
    }
}
