<?php

namespace App\Controller;

use App\Controller\AppController;

class UsersController extends AppController
{

    public function initialize(): void {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['login', 'signin', 'logout']);
    }


    public function index()
    {
        $ipclient = $this->getRequest()->clientIp();
        
        dd($ipclient, $this->getRequest()->getSession()->read('User'));
    }

    public function login()
    {
        $user = $this->Users->newEmptyEntity();

        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $target = $this->Authentication->getLoginRedirect() ?? '/';
            return $this->redirect($target);
        }
        if ($this->getRequest()->is('Post') && !$result->isValid()) {
            $this->Flash->error('Mot de passe invalide');
        } 
        





        $this->set(compact('user'));
    }

    public function signin()
    {
        $userEntity = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $this->Users->patchEntity($userEntity,$this->getRequest()->getData());
            if ($this->Users->save($userEntity)) {
                $this->Flash->success('Inscription réussis');
            }
            else {
                $this->Flash->error('Une erreur est survenue');
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('userEntity'));
    }


    public function logout()
    {
        $this->Authentication->logout();
        return $this->redirect('/users/login');
    }
}