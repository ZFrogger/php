<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Cake\ORM\RulesBuilder;

class ImagesTable extends Table {


    public function initialize(array $config): void 
    {
        parent::initialize($config);
    }
/*
    public function buildRules(RulesBuilder $rules): RuleChecker {
        $rules->add($rules->isUnique(
            ['name'], 'Image déjà existante'
        ));
        return parent::buildRules($rules);
    }
    */
}