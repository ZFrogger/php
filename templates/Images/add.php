<?php
    $this->assign('title', "Ajout d'une image");
?>
    <main role="main">
    <?= $this->element("Images/header"); ?>
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Ajouter une image</h1>
          <p class="lead text-muted"></p>
        </div>
      </section>

    <div>
        <?= $this->Form->create($imageEntity, 
        [
            'type' => 'file'
        ]) ?>
        <?= $this->Form->control("field",
        [
            'label' => "L'Image : ",
            'type' => 'file'
        ]); ?>
        <?= $this->Form->control('name',
        [
            'label' => "Titre de l'image",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->control('description',
        [
            'label' => "Description de l'image",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->control('height',
        [
            'label' => "Hauteur de l'image",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->control('width',
        [
            'label' => "Largeur de l'image",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->button('Valider', ['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
    </div>
        

    </main>
    <?= $this->element("Images/footer"); ?>
        

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
