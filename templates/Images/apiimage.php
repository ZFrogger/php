<?php
    $this->assign('title', "ma page de l'image");
?>
    <main role="main">
    <?= $this->element("Images/header"); ?>
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading"><?= isset($MyObj) ? $MyObj->file : "Aucune Image" ?></h1>
          <p class="lead text-muted"><?= isset($MyObj) ? $MyObj->description : "Aucune Image" ?></p>
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">
        <div class="row">
        <?php 
        if (isset($MyObj)) {
              echo "<div class='col-md-12'>
              <div class='card mb-12 box-shadow'>
                <img class='card-img-top' src='" . '/img/jpg/' . $MyObj->file . "' alt='" . $MyObj->description . ">";
                echo "<div class='card-body'>" .
                "<p class='card-text'>Nom : " . ($MyObj->file ? $MyObj->file : 'Pas de Nom') . "</p>" .
                "<p class='card-text'>Description : " . ($MyObj->description ? $MyObj->description : 'Pas de description') . "</p>" .
                "<p class='card-text'>Commentaire : " . ($MyObj->comment ? $MyObj->comment : 'Pas de commentaire') . "</p>" .
                "<p class='card-text'>Auteur : " .  ($MyObj->author ? $MyObj->author : 'Pas d\'auteur') . "</p>" .
                "<p class='card-text'>Taille : " .  ($MyObj->width ? $MyObj->width : '') . "x" . ($MyObj->height ? $MyObj->height : '') . "</p>" .
                "<a id='download' href='". '/img/jpg/' . $MyObj->file . "' download='" . '/img/jpg/' . $MyObj->file . "'>Download</a>" .
                  "<div class='d-flex justify-content-between align-items-center'>
                    <small class='text-muted'>now</small>
                  </div>
                </div>
              </div>
            </div>";
            }
            
          
        
        ?>
          </div>
        </div>
      </div>

    </main>
    <?= $this->element("Images/footer"); ?>
        

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
