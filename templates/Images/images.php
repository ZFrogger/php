<?php
    $this->assign('title', "ma page d'images");
?>
    <main role="main">
    <?= $this->element("Images/header"); ?>
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Images</h1>
          <p class="lead text-muted">Les images</p>
          <p>
            <a href="?pages=<?= $current - 1 < 1 ? $current : $current - 1 ?>" class="btn btn-primary my-2"><</a>
            <a href="?pages=<?= $current + 1 ?>" class="btn btn-primary my-2">></a>
          </p>
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">
        <div class="row">
        <?php 
        if (isset($current)) {
          $cpt = 0;
          foreach ($tab3 as $value) {
            if ($cpt >= 12*$current | $cpt < 12*($current-1)) {

            }
            else {
              $tmp = json_decode($value);
              echo "<div class='col-md-4'>
              <div class='card mb-4 box-shadow'><a href='/Images/image?nom=" . $tmp->file . "'>
                <img class='card-img-top' src='";
                echo '/img/jpg/' . $tmp->file;
                echo "' alt='";
                echo $tmp->description;
                echo "' style='width:100%; height:300px'></a>
                <div class='card-body'>
                  <p class='card-text'>";
                echo $tmp->description ? $tmp->description : 'Pas de description';
                echo "</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <small class='text-muted'>now</small>
                  </div>
                </div>
              </div>
            </div>";
            }
            $cpt++;
            
          }
        }
        ?>
          </div>
        </div>
      </div>

    </main>
    <?= $this->element("Images/footer"); ?>
        

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
