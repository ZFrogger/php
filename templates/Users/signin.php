<?php
    $this->assign('title', "Ma page de sign in");
    $this->assign('description', "Description de ma page de sign in");
?>
    <main role="main">
    <?= $this->element("Images/header"); ?>
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Sign in</h1>
          <p class="lead text-muted"></p>
        </div>
      </section>

    <div>
        <?= $this->Form->create($userEntity) ?>
        <?= $this->Form->control('email',
        [
            'label' => "Votre adresse email",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->control('password',
        [
            'label' => "Votre mot de passe",
            'class' => 'form-control'
        ]) ?>
         <?= $this->Form->control('name',
        [
            'label' => "Votre prénom",
            'class' => 'form-control'
        ]) ?>
         <?= $this->Form->control('lastname',
        [
            'label' => "Votre nom",
            'class' => 'form-control'
        ]) ?>
        <?= $this->Form->button('S\'enregistrer', ['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
    </div>
        

    </main>
    <?= $this->element("Images/footer"); ?>
        

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
